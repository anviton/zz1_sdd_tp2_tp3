/**
 * @file arbres_construct.c
 * @brief fichier d'implementation du programme pour la construction d'une arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_construct.h"

/**
 * @brief lire le fichier contenant la representation prefixee de l'arborescence
 * @param [in] fileName le nom du fichier contenant la representation prefixee
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref l'adresse memoire contenant le nombre des elements du tabEltPref
 * @return le nombre de racines
 */
int lirePref_fromFileName(char * fileName, eltPrefPostFixee_t * tabEltPref, int * nbEltsPref)
{
    //flux utilisé pour lire un fichier
    FILE *flot;
    //entier nbRacines utilisé pour stocker le nombre de racine de l'arbre contenu dans le fichier
    int nbRacines = 0;

    *nbEltsPref = 0;
    //ouverture du fichier en écriture
    flot = fopen(fileName, "r");
    if(flot != NULL){
        //lecture du premier entier du fichier
        fscanf(flot, "%d%*c", &nbRacines);

        //tant que nous ne sommes pas en fin de fichier
        while(!feof(flot)){
            //à chaque fois nous lisons une valeur et son nombre de fils et on la place dans la case active du tableau
            fscanf(flot, "%c %d%*c ", &tabEltPref[*nbEltsPref].val, &tabEltPref[*nbEltsPref].nbFils);
            //on incrémente le nombre d'éléments du tableau
            (*nbEltsPref)++;
        }
        fclose(flot);
    }
    return nbRacines;
}

/** TO DO
 * @brief afficher les elements de la representation prefixee sur un flux de sortie
 * @param file : le flux de sortie
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref le nombre des elements du tabEltPref
 */
void printTabEltPref(FILE *file, eltPrefPostFixee_t *tabEltPref, int nbEltsPref)
{
    //entier utilisé en tant qu'index pour parcourir le tableau
    int i;
    //on parcourt tous les élément sauf le dernier et on les affiche les un à coé des autres
    for(i = 0; i < nbEltsPref; i++){
        fprintf(file, "(%c,%d) ", tabEltPref[i].val, tabEltPref[i].nbFils);
    }
    //on affiche le dernier élément après lequel on met un saut de ligne
    fprintf(file, "\n");

}

/** TO DO
 * @brief creer et initialiser un nouveau point de l'arborescence
 * @param [in] val la valeur du point
 * @return l'adresse du nouveau point 
 */
cell_lvlh_t * allocPoint(char val)
{
    //pointeur sur une cell_lvlh_t va servir pour la création d'un nouveau noeud
    cell_lvlh_t *cell;
    //allocation d'un nouveau noeud
    cell = (cell_lvlh_t*)malloc(sizeof(cell_lvlh_t));
    //si l'allocation a fonctionné on place la valeur de champ val du noeud créé
    if(cell != NULL){
        //on place la valeur de champ val du noeud créé
        cell->val = val;
        //on met à nul le pointeur du premier fils du noeud créé
        cell->lv = NULL;
        //on met à nul le pointeur sur le premier frère du noeud créé
        cell->lh = NULL;
    }
    return cell;
}

/** TO DO
 * @brief construire un arbre avec lvlh a partir de representation prefixee
 * @param [in] tabEltPref tableau des elements de la representation prefixee
 * @param [in] nbRacines nombre de racines de l'arborescence
 * @return : 
 *     - NULL si l'arbre resultatnt est vide
 *     - l'adresse de la racine de l'arbre sinon
*/
cell_lvlh_t * pref2lvlh(eltPrefPostFixee_t * tabEltPref, int nbRacines){
    pile_t *pile =  initPile(NB_ELTPREF_MAX);
    cell_lvlh_t *racine = NULL;
    cell_lvlh_t **pprec = &racine;
    cell_lvlh_t *nouv;
    int nbFils_ou_Freres = nbRacines;
    int i = 0, retour;
    eltType_pile elt;

    //tant que le nb de fils ou de frere n'est pas nul et que la pile n'est pas vide
    while(nbFils_ou_Freres > 0 || !estVidePile(pile))
    {
        //si le nb de fils ou frere est supèrieur à 0
        //  on crée un nouveau noeud
        //  on le stocke dans eltType_pile dans le but de l'empiler
        //  on met à jour le nb de fils ou frere de l'elt en décrémentant la variable nbFils_ou_Freres
        //  on empile ce noeud
        //  on passe à la case suivante du tableau ainsi on stocke le nb fils de cette case dans la variable nbFils_ou_Freres
        if(nbFils_ou_Freres > 0){
            nouv = allocPoint(tabEltPref[i].val);
            *pprec = nouv;
            elt.adrCell = nouv;
            elt.nbFils_ou_Freres = nbFils_ou_Freres - 1;
            empiler(pile, &elt, &retour);
            pprec = &nouv->lv;
            nbFils_ou_Freres = tabEltPref[i].nbFils;
            i++;
        }
        else{//sinon on verifie que la pile n'est pas vide
            //  si elle n'est pas vide on dépile le premier élément
            //  on stocke l'@ de du lien horizontal de adrcell de l'elt dépilécdans pprec
            //  dans la variable nbFils_ou_Freres on stocke le nombre de fils ou de frere de l'elt dépilé
            if(!estVidePile(pile)){
                depiler(pile, &elt, &retour);
                pprec = &elt.adrCell->lh;
                nbFils_ou_Freres = elt.nbFils_ou_Freres;
            }
        }
    }
    //libération de la pile
    libererPile(&pile);
    return racine;
}




/** TO DO
 * @brief liberer les blocs memoire d'un arbre
 * @param [in] adrPtRacine l'adresse du pointeur de la racine d'un arbre
 */
void libererArbre(cell_lvlh_t ** adrPtRacine)
{
    //entier qui sert à stocker le code de retour de la fonction empiler
    int cd;
    //les 2 eltType_pile servent à empiler et dépiler des noeuds
    eltType_pile cour, elt_tmp;
    //pointeur temporaire sur une cell_lvlh_t qui sert à libérer un noeud
    cell_lvlh_t * temp;

    cour.adrCell = *adrPtRacine;
    pile_t *pile = initPile(NB_ELTPREF_MAX);
    
    //tant que le champs adrcell de l'eltType_pile courant n'est pas null 
    //(ce qui signifie qu'il reste au moins un noeud à dépiler)
    while (cour.adrCell)
    {
        //si le lien vertical de l'adrcell de l'eltType_pile courant n'est pas null
        //(ce qui signifie tant que le noeud stocké dans cour a au moins un fils)
        if (cour.adrCell->lv)
        {
            elt_tmp.adrCell = cour.adrCell->lv;
            //on empile le premier fils du noeud stocké dans cour
            empiler(pile, &elt_tmp, &cd);
        }
        //ici on libère le noeud pointé par cour.adrCell
        //pour cela on utilise un noeud temporaire pour ne pas perdre les frères du noeud pointé par cour.adrCell
        temp = cour.adrCell;
        cour.adrCell = cour.adrCell->lh;
        free(temp);
        
        //on vérifie si cour.adrcell pointe sur un noeud et si la pile n'est pas vide
        //  si c'est le cas on dépile un élément
        if (!cour.adrCell && !estVidePile(pile))
        {
            depiler(pile, &cour, &cd);
        }
    }
    *adrPtRacine = NULL;
    //libération de la pile
    libererPile(&pile);
}

/* Fonctions pour tests */
/** 
 * @brief Affichage prefixe de l'abre dont la racine est passée en paramètre
 * @param [in] racine l'adresse du pointeur de la racine d'un arbre
 */
void affichePrefArbre(cell_lvlh_t * racine)
{
    if (racine){
        printf("%c", racine->val);
        affichePrefArbre(racine->lv);
        affichePrefArbre(racine->lh);
    }
    
}
/** 
 * @brief Affichage prefixe dans un flux de sortie passé en paramètre de l'abre dont la racine est passée en paramètre
 * @param [in] racine l'adresse du pointeur de la racine d'un arbre
 * @param [in] file flux de sortie
 */
void affichePrefArbre_Fichier(FILE * file, cell_lvlh_t * racine){
    if (racine){
        fprintf(file, "%c ", racine->val);
        affichePrefArbre_Fichier(file, racine->lv);
        affichePrefArbre_Fichier(file, racine->lh);
    }
}