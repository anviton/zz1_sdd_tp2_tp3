/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_construct.h"
#include "../test/teZZt.h"


BEGIN_TEST_GROUP(ARBRE_CONSTRUCT)

TEST(nouvCell) {
	cell_lvlh_t *new;

	//Vérification création d'un noeud contenant la valeur 'A'
	printf("Vérification création d'un noeud contenant la valeur 'A'\n");
	new = allocPoint('A');
	REQUIRE( NULL != new );
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	free(new);
}


TEST(lirePref_fromFileName_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);

	printf("\033[34m\nlirePref_fromFileName_exTP :");
	printf("\033[0m\n");

	//Verification que l'arbre est lu correctement
	printf("Verification que l'arbre est lu correctement\n");
	CHECK( 2 == nbRacines ); 
	CHECK( 13 == nbEltsPref );

	//Vérification val exactes (val et nbFils) pour la case 0
	printf("Vérification val exactes (val et nbFils) pour la case 0\n");
	CHECK( 'A' == tabEltPref[0].val );
	CHECK( 3 == tabEltPref[0].nbFils );

	//Vérification val exactes (val et nbFils) pour la case 1
	printf("Vérification val exactes (val et nbFils) pour la case 1\n");
	CHECK( 'B' == tabEltPref[1].val );
	CHECK( 2 == tabEltPref[1].nbFils );

	//Vérification val exactes (val et nbFils) pour la case 7 (au milieu du tableau)
	printf("Vérification val exactes (val et nbFils) pour la case 7 (au milieu du tableau)\n");
	CHECK( 'C' == tabEltPref[7].val );
	CHECK( 2 == tabEltPref[7].nbFils );
	
	//Vérification val exactes (val et nbFils) pour la dernière case du tableau
	printf("Vérification val exactes (val et nbFils) pour la dernière case du tableau\n");
	CHECK( 'I' == tabEltPref[nbEltsPref-1].val );
	CHECK( 0 == tabEltPref[nbEltsPref-1].nbFils );
}


TEST(printTabEltPref_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];

	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[34m\nprintPref_exTP :");
	printf("\033[0m\n");

	//Verification affichage d'un arbre stocké dans un tableau
	printf("Verification affichage d'un arbre stocké dans un tableau (arbre de l'énoncé)\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	printf("%s", buffer);
	CHECK( 0 == strcmp(buffer, "2 (A,3) (B,2) (E,0) (J,0) (D,0) (H,1) (G,0) (C,2) (F,3) (K,0) (M,0) (T,0) (I,0) \n") );

	//Verification affichage avec un arbre à 3 racines (permet également de vérifier si la lecture d'un arbre à 3 racines fonctionnent)
	printf("Verification affichage d'un arbre stocké dans un tableau (arbre à 3 racines)\n");
	nbRacines = lirePref_fromFileName("../3racines.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	printf("%s", buffer);
	CHECK( 0 == strcmp(buffer, "3 (A,0) (B,0) (C,0) \n") );

	//Test lecture d'un arbre à un seul noeud à partir d'un fichier
	printf("Test lecture d'un arbre à un seul noeud à partir d'un fichier:\n");
	nbRacines = lirePref_fromFileName("../1racine.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	printf("%s", buffer);
	CHECK( 0 == strcmp(buffer, "1 (A,0) \n") );

	//Verification avec un arbre vide (permet également de vérifier si la lecture d'un arbre vide fonctionnent)
	printf("Verification affichage d'un arbre vide\n");
	nbRacines = lirePref_fromFileName("../vide.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	printf("%s", buffer);
	CHECK( 0 == strcmp(buffer, "0 \n") );

	//Vérification avec fichier qui n'existe pas
	printf("Verification avec un fichier qui n'existe pas\n");
	nbRacines = lirePref_fromFileName("../toto.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	printf("%s", buffer);
	CHECK( 0 == strcmp(buffer, "0 \n") );
}

// 2  (A,3)  (B,2)  (E,0)  (J,0)  (D,0)  (H,1) (G,0)  (C,2)  (F,3)  (K,0)  (M,0)  (T,0)  (I,0)
TEST(pref2lvlh1_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[35m\npref2lvlh1_exTP :");
	printf("\033[0m\n");

	//Test lecture de l'arbre de l'énnoncé à partir d'un fichier
	printf("Test lecture de l'arbre de l'énnoncé à partir d'un fichier :\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	libererArbre(&racine);
	printf("Test : %s\n", buffer);
	CHECK (0 == strcmp(buffer, "A B E J D H G C F K M T I "));

	//Test lecture d'un abre à patir d'un fichier vide
	printf("Lecture à partir d'un fichier vide :\n");
	nbRacines = lirePref_fromFileName("../vide.txt", tabEltPref, &nbEltsPref);
	CHECK(nbRacines == 0);
	racine = pref2lvlh(tabEltPref, nbRacines);
	CHECK(racine == NULL);

	//Test lecture d'un arbre à un seul noeud à partir d'un fichier
	printf("Test lecture d'un arbre à un seul noeud à partir d'un fichier:\n");
	nbRacines = lirePref_fromFileName("../1racine.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	racine = pref2lvlh(tabEltPref, nbRacines);
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	libererArbre(&racine);
	printf("Test : %s\n", buffer);
	CHECK(racine == NULL);
	CHECK (0 == strcmp(buffer, "A "));

	//Test lecture d'un arbre à 3 racines seulement à partir d'un fichier
	printf("Test lecture d'un arbre à 3 racines seulement à partir d'un fichier\n");
	nbRacines = lirePref_fromFileName("../3racines.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	racine = pref2lvlh(tabEltPref, nbRacines);
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	libererArbre(&racine);
	printf("Test : %s\n", buffer);
	CHECK (0 == strcmp(buffer, "A B C "));

	//Test lecture d'un arbre à partir d'un fichier qui n'existe pas
	printf("Test lecture d'un arbre à partir d'un fichier qui n'existe pas\n");
	buffer[0]='\0';
	nbRacines = lirePref_fromFileName("../toto.txt", tabEltPref, &nbEltsPref);
	file = fmemopen(buffer, 1024, "w");
	racine = pref2lvlh(tabEltPref, nbRacines);
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	libererArbre(&racine);
	printf("Test : %s\n", buffer);
	CHECK (0 == strcmp(buffer, ""));
}


END_TEST_GROUP(ARBRE_CONSTRUCT)

int main(void) {
	RUN_TEST_GROUP(ARBRE_CONSTRUCT);
	return EXIT_SUCCESS;
}
