/**
 * @file arbres_insert.c
 * @brief fichier d'implementation du module pour l'insertion de valeur dans une arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_insert.h"

#include "../Seance1/arbres_construct.h"

/**
 * @brief rechercher un point de valeur v
 * @param [in] racine pointeur vers la racine de l'arborescence 
 * @param [in] v le caractère à chercher
 * @return 
 *   - l'adresse du point contenant v si v existe dans l'arborescence
 *   - NULL, sinon
 */
cell_lvlh_t * rechercher_v(cell_lvlh_t * racine, char v)
{
    //eltType_pile cour utilisé pour parcourir l'arbre (et donc pour empiler et dépiler des éléments dans la pile)
    eltType_pile cour;
    //entier utilisé pour stocker le code de retour de la fonction empiler
    int code;
    //on place dans cour.adrcell le pointeur vers la racine de l'arborescence
    cour.adrCell =  racine;
    //pile utilisée pour parcourir les noeuds de l'arbre
    pile_t *pile = initPile(PILE_SZ);

    //tant que le pointeur cour.adrCell n'est pas null et que la valeur du noued pointé par cour.adrCell est différente de v
    while(cour.adrCell && (cour.adrCell)->val != v)
    {
        //on empile dans la pile cour
        empiler(pile, &cour, &code);
        //on décale le pointeur de cour.adrCell sur le premier fils du noeud pointé par cour.adrCell
        cour.adrCell = cour.adrCell->lv;
        //tant que le pointeur cour.adrCell n'est pas null et que la pile n'est pas vide
        while (cour.adrCell == NULL && !estVidePile(pile)){
            //on dépile dans cours la valeur "en haut" de  la pile
            depiler(pile, &cour, &code);
            //on décale le pointeur de cour.adrCell sur le premier frère du noeud pointé par cour.adrCell
            cour.adrCell = cour.adrCell->lh;
        }
    }
    //on libère la pile
    libererPile(&pile);
    return cour.adrCell;
}

/**
 * @brief rechercher le double prec de w dans une liste de fils
 * @param [in] adrPere l'adresse du pere
 * @param [in] w la valeur a inserer
 * @return l'adresse du pointeur prec apres lequel w doit etre inseree
 */
cell_lvlh_t ** rechercherPrecFilsTries(cell_lvlh_t *adrPere, char w)
{
    //double pointeur cell_lvlh_t utilisé pour trouver le pointeur précédent de la valaur w
    cell_lvlh_t ** pprec =  NULL;
    //Si le pointeur adrPere n'est pas null
    if(adrPere != NULL){
        //on stocke dans le double pointeur pprec l'adresse du pointeur adrPere->lv
        pprec = &(adrPere->lv);
        //tant que *pprec n'est pas nul && que la valueur du noeud pointé par le pointeur pointé par pprec est
        // infèrieure à w
        while (*pprec && (*pprec)->val < w){
            //on décale pprec, il pointera désormais sur l'adresse du pointeur lh du noeud 
            //pointé par le double pointeur pprec
            pprec = &(*pprec)->lh;
        }
    }
    return pprec;
}

/**
 * @brief inserer une valeur w dans les fils d'un point de valeur v
 * @param [in] racine la racine de l'arborescence
 * @param [in] v la valeur d'un point auquel on va inserer la valeur w en fils
 * @param [in] w la valeur a inserer
 * @return 1 - insertion realisee; 0 - insertion n'a pas ete realisee
 */
int  insererTrie(cell_lvlh_t *racine, char v, char w)
{
    //entier utilisé pour le retour (0 = pas d'insertion; 1 = insertion)
    int code = 0;
    //pointeur sur une cell_lvlh_t utilisé pour stocker l'adresse du père de la valeur v passé en paramètre
    cell_lvlh_t *pere;
    //double pointeur sur une cell_lvlh_t pour stocker l'adresse du pointeur après lequel devra être inséré la valeur v
    cell_lvlh_t **pprec;
    //pointeur sur une cell_lvlh_t, va servir pour créer le noeud qui contiendra la nouvelle valeur à insérer
    cell_lvlh_t *new;
    //recherche du noeud contenant v, on stocke son adresse dans le pointeur pere
    pere = rechercher_v(racine, v);
    //si l'adresse contenu par le pointeur père n'est pas null
    if(pere != NULL){
        //recherhceh du pointeur ou devra être contenu l'adresse du nouveau noeud conteant la nouvelle valeur à insérer
        // cette adresse de pointeur sera sotcké dans le double pointeur pprec
        pprec = rechercherPrecFilsTries(pere, w);
        //allocation d'un nouveau noeud
        new = allocPoint(w);
        //si l'allocation s'est bien passé (si new n'est pas null)
        if(new){
            //on place l'adresse pointée *pprec dans lans le pointeur contenant l'aadresse du premier frère du
            //nouveau noeud créé
            new->lh = *pprec;
            //on place l'adresse du nouveau noeud dans le pointeur pointé par pprec
            *pprec = new;
            //mise à 1 du code de retour car l'insertion a eu lieu
            code = 1;
        }
    }
    return code;
}
