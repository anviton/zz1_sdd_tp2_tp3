/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "../Seance1/arbres_construct.h"
#include "arbres_insert.h"
#include "../test/teZZt.h"


BEGIN_TEST_GROUP(ARBRE_INSERT)

TEST(nouvCell) {
	cell_lvlh_t *new;

	new = allocPoint('A');
	REQUIRE( NULL != new ); 
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	free(new);
}


TEST(rechercher_v) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	cell_lvlh_t *pere = NULL;

	printf("\033[35m\nrechercher_v :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	//Recherhe d'une valeur qui n'existe pas
	printf("Recherhe d'une valeur qui n'existe pas\n");
	pere = rechercher_v(racine, 'X');
	CHECK( NULL == pere );

	//Recherche d'uen racine
	printf("Recherche d'uen racin\n");
	pere = rechercher_v(racine, 'A');
	REQUIRE( NULL != pere );
	CHECK( 'A' == pere->val );

	//Recherche d'un noeud qui a un parent, qui  a un descendant et des fères
	printf("Recherche d'un noeud qui a un parent, qui  a un descendant et des fères\n");
	pere = rechercher_v(racine, 'H');
	REQUIRE( NULL != pere );
	CHECK( 'H' == pere->val );

	//Recherche d'une feuille
	printf("Recherche d'une feuille\n");
	pere = rechercher_v(racine, 'E');
	REQUIRE ( NULL != pere);
	CHECK( 'E' == pere->val );

	libererArbre(&racine);
}

TEST(rechercherPrecFilsTries) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	cell_lvlh_t *pere = NULL;
	cell_lvlh_t **pprec = NULL;

	printf("\033[34m\nrechercherPrecFilsTries :");
	printf("\033[0m\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	//Recherche pour que *pprec pointe sur la première valeaur d'un niveau
	printf("Recherche pour que *pprec pointe sur la première valeaur d'un niveau\n");
	pere = rechercher_v(racine, 'F');
	REQUIRE( NULL != pere );
	CHECK( 'F' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'A');
	REQUIRE( NULL != *pprec );
	CHECK( 'K' == (*pprec)->val );


	//Recherche pour *pprec soit = Null équivaut à dire que *pprec pointe sur le suivant du dernier maillon d'un niveau
	printf("Recherche pour *pprec soit = Null équivaut à dire que *pprec pointe sur le suivant du dernier maillon d'un niveau\n");
	pere = rechercher_v(racine, 'A');
	REQUIRE( NULL != pere );
	CHECK( 'A' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'Z');
	CHECK( NULL == *pprec );
	
	
	//Recherche du pprec A à partir de E (E est une feuille)
	printf("Recherche du pprec A à partir de E (E est une feuille)\n");
	pere = rechercher_v(racine, 'E');
	REQUIRE( NULL != pere );
	CHECK( 'E' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'A');
	CHECK( NULL == *pprec );


	//Recherche du pprec de L à partir de F 
	printf("Recherche du pprec de L à partir de F \n");
	pere = rechercher_v(racine, 'F');
	REQUIRE( NULL != pere );
	CHECK( 'F' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'L');
	REQUIRE( NULL != *pprec );
	CHECK( 'M' == (*pprec)->val );

	
	//Recherche du pprec L à partir de W (W n'existe pas)
	printf("Recherche du pprec L à partir de W (W n'existe pas)\n");
	pere = rechercher_v(racine, 'W');
	CHECK( NULL == pere );

	pprec = rechercherPrecFilsTries(pere, 'L');
	CHECK( NULL == pprec );


	
	libererArbre(&racine);
}

TEST(insererTrie) {
int nbRacines  = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t * racine = NULL;

	int code;
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[34m\ninsererTrie :");
	printf("\033[0m\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	//Insertion entre 2 fils
	printf("Insertion entre 2 fils\n");
	code = insererTrie(racine, 'F',  'L');
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(1 == code);
	CHECK (0 == strcmp(buffer, "A B E J D H G C F K L M T I "));

	
	//Insertion fin de lien horizontal (lh)
	printf("Insertion fin de lien horizontal (lh)\n");
	file = fmemopen(buffer, 1024, "w");
	code = insererTrie(racine, 'B', 'V');
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(1 == code);
	CHECK (0 == strcmp(buffer, "A B E J V D H G C F K L M T I "));


	//Insertion à partir d'un noeud qui n'a pas de fils
	printf("Insertion à partir d'un noeud qui n'a pas de fils\n");
	code = insererTrie(racine, 'K', 'P');
	file = fmemopen(buffer, 1024, "w");
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(1 == code);
	CHECK (0 == strcmp(buffer, "A B E J V D H G C F K P L M T I "));

	//Insertion en début de lien horizontal
	printf("Insertion en début de lien horizontal\n");
	code = insererTrie(racine, 'K', 'O');
	file = fmemopen(buffer, 1024, "w");
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(1 == code);
	CHECK (0 == strcmp(buffer, "A B E J V D H G C F K O P L M T I "));

	//Insertion à partir d'une valeur qui n'existe pas dans l'arbre
	printf("Insertion à partir d'une valeur qui n'existe pas dans l'arbre\n");
	code = insererTrie(racine, 'Z', 'Y');
	file = fmemopen(buffer, 1024, "w");
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(0 == code);
	CHECK (0 == strcmp(buffer, "A B E J V D H G C F K O P L M T I "));

	//Insertion d'une valeur déjà existante (test présent même si on part du postula 
	// que chaque valeur est unique dans l'arbre (pour garder le cohérence dans le TP))
	printf("Insertion d'une valeur déjà existante (test présent même si on part du postula que chaque valeur est unique dans l'arbre (pour garder de la cohérence dans le TP))\n");
	code = insererTrie(racine, 'K', 'O');
	file = fmemopen(buffer, 1024, "w");
	affichePrefArbre_Fichier(file, racine);
	fclose(file);
	CHECK(1 == code);
	CHECK (0 == strcmp(buffer, "A B E J V D H G C F K O O P L M T I "));

	libererArbre(&racine);
}

END_TEST_GROUP(ARBRE_INSERT)

int main(void) {
	RUN_TEST_GROUP(ARBRE_INSERT);
	return EXIT_SUCCESS;
}
