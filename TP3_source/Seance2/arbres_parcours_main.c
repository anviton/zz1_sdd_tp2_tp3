/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "../Seance1/arbres_construct.h"
#include "arbres_parcours.h"
#include "../test/teZZt.h"


BEGIN_TEST_GROUP(ARBRE_PARCOURS)

TEST(nouvCell) {
	cell_lvlh_t *new;

	new = allocPoint('A');
	REQUIRE( NULL != new ); 
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	free(new);
}


TEST(getNbFils_ou_Freres) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	fclose(file);
	REQUIRE ( NULL != file);

	printf("\033[35m\ngetNbFils_ou_Freres :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	REQUIRE( NULL != racine );
	//Vérification du nombre de frère de A
	printf("Vérification du nombre de frères de A\n");
	CHECK( 2 == getNbFils_ou_Freres(racine) );     // 2 freres y compris lui-meme
	//Vérification du nombre de fils de A
	printf("Vérification du nombre de fils de A\n");
	CHECK( 3 == getNbFils_ou_Freres(racine->lv) ); // 3 fils

	REQUIRE( NULL != racine->lv );
	//Vérification du nombre de frères de B
	printf("Vérification du nombre de frères de B\n");
	CHECK( 3 == getNbFils_ou_Freres(racine->lv) );     // 3 freres y compris lui-meme
	//Vérification du nombre de fils de B
	printf("Vérification du nombre de fils de B\n");
	CHECK( 2 == getNbFils_ou_Freres(racine->lv->lv) ); // 2 fils

	REQUIRE( NULL != racine->lv->lh );
	//Vérification du nombre de fils de D
	printf("Vérification du nombre de fils de D\n");
	CHECK( 0 == getNbFils_ou_Freres(racine->lv->lh->lv) ); // 0 fils

	REQUIRE( NULL != racine->lv->lh->lh );
	//Vérification du nombre de fils de H
	printf("Vérification du nombre de fils de H\n");
	CHECK( 1 == getNbFils_ou_Freres(racine->lv->lh->lh->lv) ); // 1 fils

	libererArbre(&racine);
}

TEST(printPostfixee) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[35m\nprintPostFixee :");
	printf("\033[0m\n");

	//Affichage de l'arbre de la consigne
	printf("Affichage de l'arbre de la consigne\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
	printPostfixee(stdout, racine);
	printPostfixee(file, racine);
	fclose(file);
	CHECK( 0 == strcmp(buffer,"(E,0) (J,0) (B,2) (D,0) (G,0) (H,1) (A,3) (K,0) (M,0) (T,0) (F,3) (I,0) (C,2) 2\n") );
	libererArbre(&racine);

	//Affichage d'un arbre à 3 racines
	printf("Affichage d'un arbre à 3 racines\n");
	nbRacines = lirePref_fromFileName("../3racines.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
	file = fmemopen(buffer, 1024, "w");
	printPostfixee(stdout, racine);
	printPostfixee(file, racine);
	fclose(file);
	CHECK( 0 == strcmp(buffer,"(A,0) (B,0) (C,0) 3\n") );
	libererArbre(&racine);

	//Affichage d'un arbre vide
	printf("Affichage d'un arbre vide\n");
	nbRacines = lirePref_fromFileName("../vide.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
	file = fmemopen(buffer, 1024, "w");
	printPostfixee(stdout, racine);
	printPostfixee(file, racine);
	fclose(file);
	CHECK( 0 == strcmp(buffer,"0\n") );
	
	libererArbre(&racine);
}


END_TEST_GROUP(ARBRE_PARCOURS)

int main(void) {
	RUN_TEST_GROUP(ARBRE_PARCOURS);
	return EXIT_SUCCESS;
}
