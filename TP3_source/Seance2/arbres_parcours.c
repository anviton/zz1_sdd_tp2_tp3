/**
 * @file arbres_parcours.c
 * @brief fichier d'implementation du module pour le parcours d'arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_parcours.h"


/** TO DO
 * @brief calculer le nombre de fils ou freres d'un point a partir du pointeur du debut de la liste
 * @param [in] ptCell pointeur vers le 1er frere
 * @return le nombre de fils ou freres
 */
int getNbFils_ou_Freres(cell_lvlh_t * ptCell)
{
    //pointeur sur une cell_lvlh_t servant à parcourir les fils d'un noeud
    cell_lvlh_t * tmp;
    //entier servant à compter le nombre de fils ou de frères
    int compteur = 0;
    tmp = ptCell;
    //tant que le pointeur tmp n'est pas nul
    while(tmp){
        //on décale tmp, tmp pointera désormais sur le frère du noeud pointé précédemment
        tmp = tmp->lh;
        //on incrémente le compteur
        compteur++;
    }
    return compteur;
    
}


/** TO DO
 * @brief parcours en profondeur postfixee
 * @param [in] file le flux de sortie
 * @param [in] racine la racine de l'arborescence
 */
void printPostfixee(FILE* file, cell_lvlh_t * racine)
{
    //entier utilisé pour stocker le code de retour de la fonction empiler
    int return_code=0;
    //eltType_pile cour utilisé pour parcourir l'arbre (et donc pour empiler et dépiler des éléments dans la pile)
    eltType_pile cour;
    cour.adrCell = racine; 
    //pile utilisée pour parcourir les noeuds de l'arbre
    pile_t * pile = initPile(NB_ELTPREF_MAX);
    //tant que cour.adrCell pointe sur un noeud
    while (cour.adrCell != NULL)
    {
        //tant que le noeud pointé par adrCell à un fils
        while(cour.adrCell->lv != NULL)
        {
            //on empile ce noeud
            empiler(pile, &cour, &return_code);
            //on décale le poiteur sur le premier fils de ce noeud
            cour.adrCell = cour.adrCell->lv;
        }
        //on affiche la valeur du noeud pointé par cour.adrCell
        fprintf(file, "(%c,%d) ",cour.adrCell->val, getNbFils_ou_Freres(cour.adrCell->lv));
        //on décale le poiteur sur le premier frère de ce noeud
        cour.adrCell = cour.adrCell->lh;

        //tant que cour.adrCell pointe sur un noeud et que la pile n'est pas vide
        while (cour.adrCell == NULL && !estVidePile(pile))
        {
            //on dépile le premier noeud de la pile
            depiler(pile, &cour, &return_code);
            //on affiche la valeur du noeud pointé par cour.adrCell
            fprintf(file, "(%c,%d) ",cour.adrCell->val, getNbFils_ou_Freres(cour.adrCell->lv));
            //on décale le poiteur sur le premier frère de ce noeud
            cour.adrCell = (cour.adrCell)->lh;
        }
    }
    fprintf(file, "%d\n", getNbFils_ou_Freres(racine));
    //on libère la pile
    libererPile(&pile);
}
