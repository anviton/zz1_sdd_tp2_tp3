/**
 * @file file.c
 * @brief fichier d'implementation pour la gestion de file
 */
#include <stdlib.h>
#include "file.h"

/**  TO DO
 * @brief Initialiser une file du type eltType
 *   - allocation de memoire pour une structure file_t et un tableau de taille elements
 * @param [in] taille taille de la file
 * @return l'adresse de la structure file_t
 */
file_t * initFile(int taille){
    file_t *file =  NULL;
    if(taille > 0){
        file = (file_t*)malloc(sizeof(file_t));
        if(file != NULL){
            file->taille = taille;
            file->nbElts = 0;
            file->base = (eltType*)malloc(taille*sizeof(eltType));
            file->base = &(file->base[0]);
            file->deb = &(file->base[0]);
            file->fin = &(file->base[taille - 1]);
        }
    }
    return file;
}


/**  TO DO
 * @brief Verifier si la file est vide (aucun element dans la file)
 * @param [in] ptFile l'adresse de la structure file_t
 * @return 1 - vide, ou 0 - non vide
 */
int estVideFile(file_t * ptFile)
{
    return (ptFile->nbElts == 0);
}


/** TO DO
 * @brief Verifier si la file est pleine
 * @param [in] ptFile l'adresse de la structure file_t
 * @return 1 - pleine, ou 0 - pas pleine
 */
int estPleineFile(file_t * ptFile){
    return (ptFile->nbElts == ptFile->taille);
}


/** TO DO
 * @brief Liberer les memoires occupees par la file
 * @param [in, out] adrPtFile l'adresse du pointeur de la structure file_t
 */
void libererFile(file_t ** adrPtFile)
    {
        if(*adrPtFile !=  NULL){
            (*adrPtFile)->base = NULL;
            (*adrPtFile)->deb = NULL;
            (*adrPtFile)->fin = NULL;
            free((*adrPtFile)->base);
            free(*adrPtFile);
            *adrPtFile = NULL;
        }
    }   


/** TO DO
 * @brief Entrer un element dans la file
 * @param [in] ptFile le pointeur de tete de la file
 * @param [in] ptVal l'adresse de la valeur a empiler
 * @param [out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void entrerFile(file_t * ptFile, eltType * ptVal, int *code)
{
    *code = 1;
    if (ptFile && !estPleineFile(ptFile)){ 
        ptFile->base[ptFile->nbElts] = *ptVal;
        ptFile->fin = &ptFile->base[ptFile->nbElts];
        ptFile -> nbElts++;
        *code = 0;
    }
}

/** TO DO
 * @brief Sortir un element de la file
 * @param [in] ptFile le pointeur de tete d'une file
 * @param [out] ptRes l'adresse de l'element sorti
 * @param [out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void sortirFile(file_t * ptFile, eltType * ptRes, int *code)
{
    *code = 1;
    if(!estVideFile(ptFile)){
        copyElt(ptFile->deb, ptRes);
        if(ptFile->deb ==  &(ptFile->base[ptFile->taille-1])){
            ptFile->deb = ptFile->base;
        }
        else{
            ptFile->deb = ptFile->deb + 1;
        }
        ptFile->nbElts--;
    *code = 0;
    }
    
}
